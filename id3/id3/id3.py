import os
import numpy as np
import math
from node import Node
import csv
from sklearn.model_selection import KFold

"""
Creates a decision tree of the training set using ID3 algorithm
Usage:
    create a new Id3 object:
        id3 = Id3()
        
    to start a kfold cross validation with CSV in the script directory:
        id3.kfold_validation()
    The script will ask to select a dataset, a parameter for k and it will
    compute the error rate
    
    load a CSV training set:
        id3.load_from_csv("file.csv")
    
    OR you can generate a random boolean training set with m examples and n 
    attributes:
        id3.generate_random_ts(m, n)
    
    To calculate the decision tree and store it in a variable:
        tree = id3.id3_calc(id3.training_set)
        
    To show the decision tree:
        Node.print_tree(tree)
    
"""


class Id3:

    def __init__(self):
        self.training_set = None
        self.attributes_to_process = None
        self._cached_training_set = None

    """ 
    Load a training set with optional labels
    Params:
    training_set: the training set used to calculate the DT
       m x n matrix of values. m examples, n-1 attributes
       [m,0] is the label of the example n
       [0,1] ... X[m,n] is the value of the attribute
    labels: 1 x n vector with labels of the attributes ([0] is the name
       of label)
    """
    def load_ts(self, training_set, labels=None):
        if training_set is not None:
            self.training_set = np.array(training_set)
        self._update_labels(labels)

    """
    Update labels description
    Params:
    labels_passed: 1 x n vector with labels of the attributes ([0] is the
    name of label)
    """
    def _update_labels(self, labels_passed):
        if labels_passed is not None:
            self.labels = labels_passed
        else:
            self.labels = self._create_attributes_labels(
                self.training_set.shape[1])

    """
    ID3 calculation
    Params:
    training_set: the training set to calculate this DT
    attributes_to_process: list of attributes not yet processed
    Returns:
    A node object
    """
    def _id3_calc(self, training_set, attributes_to_process):
        # create new empty node -> node
        node = Node(None)

        # if all labels of the training set are the same return node as a leaf
        # with such value as label
        if Id3._check_training_set_same_label(training_set):
            node.label = training_set[0, 0]
            return node

        # if attributes_to_process is empty, return node as a leaf with the
        # most common value for label
        if len(attributes_to_process) == 0:
            node.label = self._most_common_label(training_set)
            return node

        # find attribute with best information gain (best_attribute_idx)
        best_attribute_idx = self._best_information_gain(training_set,
                                                         attributes_to_process)
        if best_attribute_idx == 0:
            node.label = Id3._most_common_label(training_set)
            return node

        # node get best_attribute_idx as label
        node.label = self.labels[best_attribute_idx]

        # create branches on node for every value of the attribute
        # values = np.unique(training_set[:, best_attribute_idx])
        values = np.unique(self._cached_training_set[:, best_attribute_idx])

        # for every branch
        for v in values:

            # create subset_training_set with only rows with
            # best_attribute_idx branch value
            subset_training_set = training_set[
                                  training_set[:, best_attribute_idx] == v, :]

            # if subset_training_set is empty, put a leaf on this branch with
            #   most common value for label
            if subset_training_set.size == 0:
                leaf = Node(self._most_common_label(training_set))
                node.add_child(leaf, v)

            # else remove best_attribute_idx from attributes_to_process
            else:
                if best_attribute_idx in attributes_to_process:
                    del attributes_to_process[best_attribute_idx]
                # create new empty node -> new_node
                new_node = self._id3_calc(subset_training_set,
                                          attributes_to_process)
                # attach new_node to this branch of node
                node.add_child(new_node, v)
        return node

    def id3_start(self, training_set):
        self._cached_training_set = training_set
        return self._id3_calc(training_set, self.create_attributes_to_process(
                                     training_set))

    """
    Check if all the training set contains the same label for all examples
    Params:
    training_set: the training to check
    Returns:
    boolean
    """
    @staticmethod
    def _check_training_set_same_label(training_set):
        training_set_t = training_set.T
        if np.all(training_set_t[0] == training_set_t[0][0]):
            return True
        else:
            return False

    """
    Finds the most common value for the label in the training set
    Params:
    training_set: the training set to check
    Returns:
    a label
    """
    @staticmethod
    def _most_common_label(training_set):
        unique, counts = np.unique(training_set[:, 0], return_counts=True)
        v = np.argmax(counts)
        return unique[v]

    """
    Calculates the entropy of a training set
    Params:
    training_set: the training set to calculate the entropy
    Returns:
    real
    """
    @staticmethod
    def _entropy_ts(training_set):
        temp = 0
        training_set_size = training_set.shape[0]
        unique, counts = np.unique(training_set[:, 0], return_counts=True)
        for c in counts:
            p = c / training_set_size
            temp += p * math.log2(p)
        return temp * (-1)

    """
    Calculates the information_gain of an attribute
    Params:
    training_set: the training set
    attribute: the attribute to calculate the IG to
    Returns:
    real
    """
    @staticmethod
    def _information_gain(training_set, attribute):
        iga = 0
        training_set_size = training_set.shape[0]
        entropy_training_set = Id3._entropy_ts(training_set)
        result = np.unique(training_set[:, attribute], return_counts=True)
        for idx in range(len(result[0])):
            v = result[0][idx]
            c = result[1][idx]
            training_set_att_v = training_set[
                                 training_set[:, attribute] == v, :]
            att_entropy_v = Id3._entropy_ts(training_set_att_v)
            iga -= c / training_set_size * att_entropy_v
        iga = entropy_training_set + iga
        return iga

    """
    Returns the attribute with the best information gains
    Params:
    training_set: the training set
    attributes_to_process: the list of attributes to check
    Returns:
    the attribute with the best IG
    """
    @staticmethod
    def _best_information_gain(training_set, attributes_to_process):
        max_igs_idx = 0
        max_igs = 0
        for v in attributes_to_process.values():
            temp = Id3._information_gain(training_set, v)
            if temp > max_igs:
                max_igs = temp
                max_igs_idx = v

        return max_igs_idx

    """
    Creates a list of the attributes to process
    Params:
    training_set: the training set
    Returns:
    a dictionary with the attributes to process
    """
    @staticmethod
    def create_attributes_to_process(training_set):
        attr_dict = {}
        for att in range(1, training_set.shape[1]):
            attr_dict[att] = att
        return attr_dict

    """
    Creates a list of generic labels
    Params:
    training_set: the training set size
    Returns:
    an array with the generic labels
    """
    @staticmethod
    def _create_attributes_labels(training_set_cols):
        labels_tmp = ["Label"]
        for att in range(1, training_set_cols):
            labels_tmp.append("X" + str(att))
        return labels_tmp

    """
    Loads a training set from a CSV
    Params:
    file: the file location complete with directory
    Returns:
    a training set
    """
    def load_from_csv(self, file):
        first = True
        training_set = []
        with open(file, newline='') as csvfile:
            ts_file = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in ts_file:
                b = [s.split(',') for s in row]
                if first:
                    labels = b[0]
                    first = False
                else:
                    training_set.append(b[0])
        self.load_ts(training_set, labels)

    """
    Creates a random training set of boolean values
    Params:
    n_examples: number of examples
    n_attributes: number of attributes
    Returns:
    a training set
    """
    def generate_random_ts(self, n_examples, n_attributes):
        values = [0, 1]
        self.training_set = \
            np.random.choice(values, size=(n_examples, n_attributes + 1))
        self._update_labels(None)

    """
    Traverse a tree in order to find a classification for a attribute 
    vector
    Params:
    tree: the tree to traverse
    attributes: an attribute vector
    Returns:
    a classification
    """
    def traverse_tree(self, tree, attributes):
        if not bool(tree.children):
            return tree.label
        else:
            attr_index = self.labels.index(tree.label)
            if attributes[attr_index] in tree.children:
                next_node = tree.children[attributes[attr_index]]
                return self.traverse_tree(next_node, attributes)
            else:
                return None

    """
    Prompts for a CSV file
    Returns:
    a file to load
    """
    @staticmethod
    def select_dataset():
        n = 0
        files = {}
        print("Select a data set: ")
        for file in os.listdir("./"):
            if file.endswith(".csv"):
                n += 1
                print(str(n) + " ", file)
                files[str(n)] = file
        sel = input("Insert a number: ")
        return files[sel]

    """
    Executes a kfold validation test on a dataset 
    """
    def kfold_validation(self):
        # select file
        file = self.select_dataset()
        self.load_from_csv(file)

        # rand ts
        rng = np.random.default_rng()
        rng.shuffle(self.training_set)

        # ask k
        n_examples = self.training_set.shape[0]
        n_attributes = self.training_set.shape[1]
        print("Select a k value (this dataset has " + str(n_examples) +
              " examples and " + str(n_attributes) + " attributes): ")
        k = int(input())
        if k > n_examples:
            k = n_examples

        # execute kfold
        kf = KFold(n_splits=k)
        sum_errors = 0
        n_ts = 1
        for train_index, test_index in kf.split(self.training_set):
            print("Training set " + str(n_ts) + " of " + str(k))
            training_set = self.training_set[train_index]
            test_set = self.training_set[test_index]
            print("Generating tree...")
            tree = self.id3_start(training_set)
            sum_errors += self.validation(tree, test_set)
            n_ts += 1
        error_rate = 1 / n_examples * sum_errors
        print("Error rate: " + str(error_rate))

    """
    Execute a validation on a single tree / test set pair
    Params:
    tree: the ID3 tree to test
    test_set: the test set of examples
    Returns:
    number of errors
    """
    def validation(self, tree, test_set):
        errors = 0
        test_set_size = test_set.shape[0]
        for sample in test_set:
            result = self.traverse_tree(tree, sample)
            expected = sample[0]
            if result != expected:
                errors += 1
        print("Found " + str(errors) + " errors on " + str(test_set_size) +
              " examples")
        return errors
