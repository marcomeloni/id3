

# class to represent a tree
# label is the name of the node (value of attribute or leaf)
# children are stored in a dictionary such as {'value1'=> node,
# 'value2' =>..., }
class Node:
    def __init__(self, label):
        if label is not None:
            self.label = label
        self.father = None
        self.children = {}

    def add_child(self, child, value):
        self.children[value] = child
        child.father = self

    def add_branch(self, value):
        self.children[value] = None

    def print_tree(self, space=0):
        print(self.label)
        if self.children is not None:
            space += 10
            for value, child in self.children.items():
                self._print_spaces(space)
                print("|- " + str(value) + " - ", end='')
                child.print_tree(space)

    @staticmethod
    def _print_spaces(n_spaces):
        for _ in range(n_spaces):
            print(" ", end='')
