# Project Title

ID3 Decision Tree generator

# Usage

Create an ID3 object:

```
    id3 = Id3()
```

To import a CSV with your training set

```
    id3.load_from_csv("opticalDigit.csv")
```

Or to generate a random boolean training set (useful for testing) with *m* examples and *n* attributes

```
    id3.generate_random_ts(m, n)
```

Calculate the tree with:

```
    tree = id3.id3_calc(id3.training_set)
```

Resulting decision tree will be stored in *tree* variable. To show the tree:

```
    Node.print_tree(tree)
```

## Authors

* **Marco Meloni**
